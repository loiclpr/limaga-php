-- phpMyAdmin SQL Dump
-- version 4.1.14.8
-- http://www.phpmyadmin.net
--
-- Client :  localhost
-- Généré le :  Lun 18 Janvier 2016 à 21:29
-- Version du serveur :  5.1.73
-- Version de PHP :  5.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `leprieur1u`
--

-- --------------------------------------------------------

--
-- Structure de la table `abonnement`
--

CREATE TABLE IF NOT EXISTS `abonnement` (
  `id_article` int(5) NOT NULL,
  `id_user` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id_article`),
  KEY `id_user` (`id_user`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Structure de la table `article`
--

CREATE TABLE IF NOT EXISTS `article` (
  `id_article` int(5) NOT NULL,
  `nom` varchar(20) COLLATE utf8_bin NOT NULL,
  `prix` decimal(5,2) DEFAULT NULL,
  `description` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id_article`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Contenu de la table `article`
--

INSERT INTO `article` (`id_article`, `nom`, `prix`, `description`) VALUES
(1, 'Billet adulte', '5.00', 'Entrée adulte valable pendant une journée pour la '),
(2, 'Cours individuel', '15.00', 'Cours individuel pendant une heure avec un maitre ');

-- --------------------------------------------------------

--
-- Structure de la table `billet`
--

CREATE TABLE IF NOT EXISTS `billet` (
  `id_article` int(5) NOT NULL,
  `date` date DEFAULT NULL,
  `quantite` int(2) DEFAULT NULL,
  `id_abonnement` int(5) DEFAULT NULL,
  PRIMARY KEY (`id_article`),
  KEY `id_abonnement` (`id_abonnement`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Contenu de la table `billet`
--

INSERT INTO `billet` (`id_article`, `date`, `quantite`, `id_abonnement`) VALUES
(1, '2005-01-16', 1, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `commande`
--

CREATE TABLE IF NOT EXISTS `commande` (
  `id_commande` int(100) NOT NULL AUTO_INCREMENT,
  `id_utilisateur` varchar(30) COLLATE utf8_bin NOT NULL,
  `montant_commande` decimal(5,2) DEFAULT '0.00',
  PRIMARY KEY (`id_commande`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `cours`
--

CREATE TABLE IF NOT EXISTS `cours` (
  `id_article` int(5) NOT NULL,
  `date_cours` date DEFAULT NULL,
  `niveau` int(1) DEFAULT NULL,
  `cours_individuel` int(1) DEFAULT NULL,
  `id_maitrenageur` int(5) DEFAULT NULL,
  PRIMARY KEY (`id_article`),
  KEY `id_maitrenageur` (`id_maitrenageur`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Contenu de la table `cours`
--

INSERT INTO `cours` (`id_article`, `date_cours`, `niveau`, `cours_individuel`, `id_maitrenageur`) VALUES
(2, '2016-01-05', 2, 1, 1);

-- --------------------------------------------------------

--
-- Structure de la table `facture`
--

CREATE TABLE IF NOT EXISTS `facture` (
  `id_facture` int(5) NOT NULL,
  `montant_a_regler` decimal(5,2) DEFAULT NULL,
  `id_commande` int(5) DEFAULT NULL,
  PRIMARY KEY (`id_facture`),
  KEY `id_commande` (`id_commande`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Contenu de la table `facture`
--

INSERT INTO `facture` (`id_facture`, `montant_a_regler`, `id_commande`) VALUES
(1, '5.00', 1);

-- --------------------------------------------------------

--
-- Structure de la table `maitreNageur`
--

CREATE TABLE IF NOT EXISTS `maitreNageur` (
  `id_maitrenageur` int(5) NOT NULL,
  `disponible` int(1) DEFAULT NULL,
  PRIMARY KEY (`id_maitrenageur`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Contenu de la table `maitreNageur`
--

INSERT INTO `maitreNageur` (`id_maitrenageur`, `disponible`) VALUES
(1, 0);

-- --------------------------------------------------------

--
-- Structure de la table `materiel`
--

CREATE TABLE IF NOT EXISTS `materiel` (
  `id_article` int(5) NOT NULL,
  `disponible` int(1) DEFAULT NULL,
  `estLouable` int(1) DEFAULT NULL,
  `date_debut` date DEFAULT NULL,
  `date_fin` date DEFAULT NULL,
  PRIMARY KEY (`id_article`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Structure de la table `panier`
--

CREATE TABLE IF NOT EXISTS `panier` (
  `id_article` int(5) NOT NULL,
  `id_commande` int(5) NOT NULL,
  `qte` int(10) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_article`,`id_commande`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Contenu de la table `panier`
--

INSERT INTO `panier` (`id_article`, `id_commande`, `qte`) VALUES
(1, 1, 1);

-- --------------------------------------------------------

--
-- Structure de la table `utilisateur`
--

CREATE TABLE IF NOT EXISTS `utilisateur` (
  `adMail` varchar(30) COLLATE utf8_bin NOT NULL,
  `liste_famille` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `nom` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `prenom` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `niveau` int(1) DEFAULT NULL,
  `mot_de_passe` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`adMail`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Contenu de la table `utilisateur`
--

INSERT INTO `utilisateur` (`adMail`, `liste_famille`, `nom`, `prenom`, `niveau`, `mot_de_passe`) VALUES
('bidule@email.com', 'Toto/Titi/Tata', 'BIDULE', 'Jean', 2, '1234');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
