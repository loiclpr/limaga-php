<?php
/**
 * Created by PhpStorm.
 * User: Loic
 * Date: 07/01/2016
 * Time: 11:36
 */
session_start();
include 'vendor/autoload.php';
require_once "conf/ConnectionFactory.php";

/* Etablissement de la connection à la DB */
\conf\ConnectionFactory::makeConnection('conf/db.config.ini');

$app = new \Slim\Slim();

$app->get('/accueil', function(){
    $controller = new \app\control\UtilisateurController();
    $controller->accueil();
});

$app->get('/deconnexion', function(){
    $controller = new app\control\UtilisateurController();
    $controller->deconnexion();
});

$app->get('/profil', function(){
    $controller = new app\control\UtilisateurController();
    $controller->profil();
});

$app->get('/catalogue', function(){
    $controller = new \app\control\UtilisateurController();
    $controller->catalogue();
});
$app->post('/catalogue', function(){
    $controller = new \app\control\UtilisateurController();
    $controller->catalogue();
});
$app->get('/paiement', function() {
    $controller = new app\control\UtilisateurController();
    $controller->payer(); // methode pour afficher la facture et des formulaires pour payer
});

$app->post('/paiement', function() {
    $controller = new app\control\UtilisateurController();
    $controller->payer(); // methode pour afficher la facture et des formulaires pour payer
});
$app->get('/panier', function() {
    $controller = new app\control\UtilisateurController();
    $controller->panier();
});

$app->post('/panier', function() {
    $controller = new app\control\UtilisateurController();
    $controller->panier();
});

$app->get('/connexion', function() {
    $controller = new app\control\ControleurAuth();
    $controller->afficherFormulaire();
});

$app->post('/connexion', function() {
    $controller = new app\control\ControleurAuth();
    $controller->afficherFormulaire();
});

$app->get('/inscription', function() {
    $controller = new app\control\ControleurInscription();
    $controller->afficherFormulaire();
});

$app->post('/inscription', function() {
    $controller = new app\control\ControleurInscription();
    $controller->afficherFormulaire();
});

$app->get('/facture', function() {
    $controller = new app\control\UtilisateurController();
    $controller->facturer();
});

$app->post('/facture', function() {
    $controller = new app\control\UtilisateurController();
    $controller->facturer();
});

$app->run();
