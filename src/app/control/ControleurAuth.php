<?php
/**
 * Created by PhpStorm.
 * User: Laura
 * Date: 12/01/2016
 * Time: 10:26
 */

namespace app\control;

use app\models\Utilisateur as Utilisateur;
use app\utils\Authentification;
use app\vues\VueAuth as VueAuth;

class ControleurAuth
{
    public function __construct(){

    }
    public function afficherFormulaire(){
        $vue = new VueAuth();
        echo $vue->formulaire();
        $this->verificationForm();

    }

    private function verificationForm(){
        if(isset($_POST['formconnexion'])) {
            $mail = $_POST['email'];
            $mdp = $_POST['mdp'];

            Authentification::authentificate($mail,$mdp);
            //$_SESSION['idProfil']= $mail;

        }
    }

}