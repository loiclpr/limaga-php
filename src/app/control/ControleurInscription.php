<?php
/**
 * Created by PhpStorm.
 * User: Laura
 * Date: 15/01/2016
 * Time: 09:08
 */

namespace app\control;
require_once "conf/ConnectionFactory.php";

use app\models\Utilisateur;
use app\utils\Authentification;
use conf\ConnectionFactory as ConnectionFactory;
use app\vues\VueInscription as VueInscription;

class ControleurInscription
{
    public function __construct()
    {

    }

    public function afficherFormulaire()
    {
        $vue = new VueInscription();
        echo $vue->formulaire();
        $this->verificationForm();

    }

    private function verificationForm()
    {

        if (isset($_POST['forminscription'])) {
            $mail = $_POST['email'];
            $nom = $_POST['nom'];
            $prenom = $_POST['prenom'];
            $niveau = $_POST['niveau'];
            $mdp = $_POST['mdp'];
            $mdp2 = $_POST['mdp2'];
            if (filter_var($mail, FILTER_VALIDATE_EMAIL)) {


                $list = Utilisateur::find($mail);

                if (!isset($list)) {
                    if ($mdp == $mdp2) {
                        $v = new Utilisateur();
                        $v->nom = $nom;
                        $v->adMail= $mail;
                        $v->prenom = $prenom;
                        $v->niveau = $niveau;
                        Authentification::creatUser($v,$mdp);
                        $v->save();


                        echo 'Votre compte a bien été créé !<a href="connexion">Me connecter</a>';


                    }else{
                        echo 'Mots de passe différents';
                    }
                }else{
                    echo 'Compte existant';
                }


            }
        }
    }
}