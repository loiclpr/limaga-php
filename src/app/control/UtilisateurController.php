<?php
/**
 * Created by PhpStorm.
 * User: Loic
 * Date: 08/01/2016
 * Time: 09:24
 */

namespace app\control;


use app\models\Article;
use app\models\Commande;
use app\models\Panier;
use app\models\Utilisateur;
use app\utils\Authentification;
use app\vues\VueAccueil;
use app\vues\VueAuth;
use app\vues\VueCatalogue;
use app\vues\VueInscription;
use app\vues\VuePanier;
use app\vues\VuePaiement;
use app\vues\VueProfil;
use app\vues\VueFacture;

class UtilisateurController
{
    public function __contruct()
    {
    }

    public function accueil(){
        $vue = new VueAccueil();
        if(isset($_SESSION['idProfil'])){
            $vue->connecte();
        }else{
            $vue->nonConnecte();
        }
    }

    public function deconnexion(){
        Authentification::deconnexion();
        $vue = new VueAccueil();
        $vue->nonConnecte();
    }

    public function profil(){
        if(isset($_SESSION['idProfil'])){
            foreach(Utilisateur::all()->where('adMail', $_SESSION['idProfil']) as $user){
               $array_info = $user['attributes'];
            }
            $vue = new VueProfil($array_info);
            $vue->render();
        }
    }

    public function catalogue()
    {
        if(isset($_SESSION['idProfil'])){
            foreach (Article::all() as $article) {
              $array_articles[] = $article;
            }
            $vue = new VueCatalogue($array_articles);
            $vue->render();
        }else{
            //$vue = new VueAuth();
            //$vue->formulaire();
            $c= new ControleurAuth();
            $c->afficherFormulaire();
        }

    }

    public function panier()
    {
        if(isset($_SESSION['idProfil'])) {
            $i = 0;
            $commande = new Commande();
            //$commande->id_commande = $idcom;
            $commande->paye = false;
            $commande->save();
            $commande->id_utilisateur = $_SESSION['idProfil'];
            $app = \Slim\Slim::getInstance();
            $sub = $app->request->post('commander');
            $res = array();
            if (isset($sub) && ($sub == 'com')) {
                $total = 0;
                foreach (Article::all() as $article) {
                    // !!!!! art represente la quantité d'articel indiqué dans le formulaire
                    $art = $app->request->post($article->id_article);
                    if (isset($art)) {
                        $tmp = filter_var($art, FILTER_SANITIZE_NUMBER_INT);
                        if ($tmp != null & $tmp != 0) {
                            $panier = new Panier();
                            $panier->id_article = $article->id_article;
                            $panier->id_commande = $commande->id_commande;
                            $panier->qte = $tmp;
                            $panier->save();
                            $total = $total + ($tmp * $article->prix);
                            $res[] = $article;
                            $res[] = $tmp;
                            $i++;
                        }
                    }
                }
                $commande->montant_commande = $total;
                if ($i == 0) {
                    $res[] = "vous n'avez command� aucun article";
                    $commande->delete();
                }
            }
            $vuePanier = new VuePanier($res);
            $vuePanier->render();
        }else{
            $c= new ControleurAuth();
            $c->afficherFormulaire();
        }
    }

    public function payer(){
        if(isset($_SESSION['idProfil'])) {
            $vuepaiement = new VuePaiement();
            $vuepaiement->render();
        }else{
            $c= new ControleurAuth();
            $c->afficherFormulaire();
        }
    }

    public function facturer(){
        if(isset($_SESSION['idProfil'])) {
            $vuef = new VueFacture();
            $vuef->render();
        }else{
            $c= new ControleurAuth();
            $c->afficherFormulaire();
        }
    }

}