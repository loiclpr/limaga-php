<?php

/**
 * Created by PhpStorm.
 * User: Loic
 * Date: 07/01/2016
 * Time: 11:38
 */
namespace app\models;
use Illuminate\Database\Eloquent\Model as Model;

class Article extends Model{
    protected $table = 'article';
    protected $primaryKey = 'id_article';
    public $timestamps = false;
}