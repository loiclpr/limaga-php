<?php
/**
 * Created by PhpStorm.
 * User: Loic
 * Date: 08/01/2016
 * Time: 09:01
 */

namespace app\models;


use Illuminate\Database\Eloquent\Model;

class Commande extends Model{
    protected $table = 'commande';
    protected $primaryKey = 'id_commande';
    public $timestamps = false;
}