<?php
/**
 * Created by PhpStorm.
 * User: pierre
 * Date: 10/01/2016
 * Time: 15:41
 */

namespace app\models;


use Illuminate\Database\Eloquent\Model;

class Panier extends Model{
    protected $table = 'panier';
    protected $primaryKey = 'id_article';
    public $timestamps = false;
}