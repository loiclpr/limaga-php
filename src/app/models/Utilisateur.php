<?php
/**
 * Created by PhpStorm.
 * User: Loic
 * Date: 08/01/2016
 * Time: 09:01
 */

namespace app\models;


use Illuminate\Database\Eloquent\Model;

class Utilisateur extends Model{
    protected $table = 'utilisateur';
    protected $primaryKey = 'adMail';
    public $timestamps = false;
}