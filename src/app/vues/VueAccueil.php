<?php
/**
 * Created by PhpStorm.
 * User: Loic
 * Date: 18/01/2016
 * Time: 22:44
 */

namespace app\vues;


class VueAccueil{

    public function __construct(){
    }

    public function nonConnecte(){
        $html=<<<END
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <title>Limaga</title>
    <link href=/www/leprieur1u/limaga-php/src/web/style.css rel=stylesheet type=text/css media=all>
</head>
<body>
    <nav>
        <ul>
            <span><a href=accueil><li><img src=/www/leprieur1u/limaga-php/src/web/images/logo-limaga.png></li></a></span>
            <a href=tarif><li>TARIF</li></a>
            <a href=catalogue><li>CATALOGUE</li></a>
            <a href=inscription><li>S'ENREGISTRER</li></a>
            <a href=connexion><li>SE CONNECTER</li></a>
        </ul>
    </nav>
    <div class=main>
        <div id=illustration-piscine>
            <img src="/www/leprieur1u/limaga-php/src/web/images/piscine-accueil.jpg" width="100" height="100" title="Bassins du complexe de Limaga">
        </div>
        <article>
            <h2>Description</h2>
            <p>Le complexe aquatique de Limaga est un centre de loisirs regroupant des services comme :
                <ul>
                    <li>La réservation de cours collectifs</li>
                    <li>La réservation de cours individuels</li>
                    <li>L'achat d'abonnements</li>
                    <li>L'achat de E-billets</li>
                    <li>L'achat de fournitures de natation</li>
                    <li>La location de matériels</li>
                </ul>
            </p>
        </article>
        <article>
            <h2>Horaires d'ouverture</h2>
            <ul>
                <li>Lundi : 10h - 20h</li>
                <li>Mardi : 12h - 20h</li>
                <li>Mercredi : 10h - 20h</li>
                <li>Jeudi : 12h - 20h</li>
                <li>Vendredi : 10h - 20h</li>
                <li>Samedi : 10h - 16h</li>
                <li>Dimanche : FERME
            </ul>
        </article>
    </div>
    <footer>
        <div id=logo-footer>
            <img src=/www/leprieur1u/limaga-php/src/web/images/raccoon-logo.png width="60" height="50">
            <h3>Raccoon & Co</h3>
        </div>
    <div class="pied"> <h5> Copyright 2015-2016 - DAUDIER_LEPRIEUR_TRIVINO - Tous droits réservés </h5> </div>
    </footer>
</body>
</html>
END;
        echo $html;
    }

    public function connecte(){
        $user = '';
        if(isset($_SESSION['idProfil'])){
            $user = $_SESSION['idProfil'];
        }

        $html=<<<END
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <title>Limaga</title>
    <link href=/www/leprieur1u/limaga-php/src/web/style.css rel=stylesheet type=text/css media=all>
</head>
<body>
    <nav>
        <ul>
            <span><a href=accueil><li><img src=/www/leprieur1u/limaga-php/src/web/images/logo-limaga.png height="100" width="100"></li></a></span>
            <a href=tarif><li>TARIF</li></a>
            <a href=catalogue><li>CATALOGUE</li></a>
            <a href=panier><li>PANIER</li></a>
            <a href=deconnexion><li>SE DECONNECTER</li></a>
            <a href=profil><li>PROFIL $user</li></a>
        </ul>
    </nav>
    <div class=main>
        <div id=illustration-piscine>
            <img src="/www/leprieur1u/limaga-php/src/web/images/piscine-accueil.jpg" width="100" height="100" title="Bassins du complexe de Limaga">
        </div>
        <article>
            <h2>Description</h2>
            <p>Le complexe aquatique de Limaga est un centre de loisirs regroupant des services comme :
                <ul>
                    <li>La réservation de cours collectifs</li>
                    <li>La réservation de cours individuels</li>
                    <li>L'achat d'abonnements</li>
                    <li>L'achat de E-billets</li>
                    <li>L'achat de fournitures de natation</li>
                    <li>La location de matériels</li>
                </ul>
            </p>
        </article>
        <article>
            <h2>Horaires d'ouverture</h2>
            <ul>
                <li>Lundi : 10h - 20h</li>
                <li>Mardi : 12h - 20h</li>
                <li>Mercredi : 10h - 20h</li>
                <li>Jeudi : 12h - 20h</li>
                <li>Vendredi : 10h - 20h</li>
                <li>Samedi : 10h - 16h</li>
                <li>Dimanche : FERME
            </ul>
        </article>
    </div>
    <footer>
        <div id=logo-footer>
            <img src=/www/leprieur1u/limaga-php/src/web/images/raccoon-logo.png width="60" height="50">
            <h3>Raccoon & Co</h3>
        </div>
        <div class="pied"> <h5> Copyright 2015-2016 - DAUDIER_LEPRIEUR_TRIVINO - Tous droits réservés </h5> </div>
    </footer>
</body>
</html>
END;
        echo $html;
    }
}