<?php
/**
 * Created by PhpStorm.
 * User: Laura
 * Date: 11/01/2016
 * Time: 20:29
 */

namespace app\vues;

define('PATH_ROOT', explode('index.php', \Slim\Slim::getInstance()->request->getRootUri())[0]);
class VueAuth
{
    public function __construct()
    {

    }

    public function formulaire(){
        $user = '';
        if(isset($_SESSION['idProfil'])){
            $user = $_SESSION['idProfil'];
        }

        $html=<<<CT
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <title>Limaga</title>
    <link href=/www/leprieur1u/limaga-php/src/web/style.css rel=stylesheet type=text/css media=all>
</head>
<body>
   <nav>
        <ul>
            <span><a href=accueil><li><img src=/www/leprieur1u/limaga-php/src/web/images/logo-limaga.png></li></a></span>
            <a href=tarif><li>TARIF</li></a>
            <a href=catalogue><li>CATALOGUE</li></a>
            <a href=inscription><li>S'ENREGISTRER</li></a>
            <a href=connexion><li>SE CONNECTER</li></a>
        </ul>
   </nav>
   <div align="center">
        <h2>Connexion</h2>
        <br /><br />
        <form method="POST" action="">
            <table>
                <tr>
                    <td align="right">
                        <label for="email">Email :</label>
                    </td>
                    <td>
                        <input type="email" required="Email requis" placeholder="Votre mail" id="email" name="email" />
                    </td>
                </tr>

                <tr>
                    <td align="right">
                        <label for="mdp">Mot de passe :</label>
                    </td>
                    <td>
                        <input type="password" required="mot de passe requis" placeholder="Votre mot de passe" id="mdp" name="mdp" />
                    </td>
                </tr>

                <tr>
                    <td></td>
                    <td align="center">
                        <br />
                        <input type="submit" name="formconnexion" value="Je me connecte" />
                    </td>
                </tr>
            </table>
        </form>
   </div>
   <footer>
        <div id=logo-footer>
            <img src=/www/leprieur1u/limaga-php/src/web/images/raccoon-logo.png width="60" height="50">
            <h3>Raccoon & Co</h3>
        </div>
    <div class="pied"> <h5> Copyright 2015-2016 - DAUDIER_LEPRIEUR_TRIVINO - Tous droits réservés </h5> </div>
    </footer>
</body>
</html>
CT;
       echo $html;
    }
}

?>