<?php
/**
 * Created by PhpStorm.
 * User: pierre
 * Date: 19/01/2016
 * Time: 21:49
 */


namespace app\vues;


use app\models\Commande;

define('PATH_ROOT', explode('index.php', \Slim\Slim::getInstance()->request->getRootUri())[0]);
class VueFacture
{

    private $tab;

    public function __construct()
    {
    }

    public function render(){
        $user = '';
        if(isset($_SESSION['idProfil'])){
            $user = $_SESSION['idProfil'];
        }

       $idutilisateur = $_SESSION['idProfil'];
        // cette ligne ne fonctionne pas
        $commande = Commande::where('paye', '=', '0')
            ->where(id_utilisateur, '=', $idutilisateur)
            ->get();
        $content = "facture numero : " . $commande->id_commande . "  montant : " . $commande->montant_commande . " euros";
        $commande->paye= true;
        $commande->save();
        $html = <<<END
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <title>Limaga</title>
    <link href=/www/leprieur1u/limaga-php/src/web/style.css rel=stylesheet type=text/css media=all>
</head>
<body>
    <nav>
        <ul>
            <a href=accueil><li>LIMAGA - ACCUEIL</li></a>
            <a href=tarif><li>TARIF</li></a>
            <a href=catalogue><li>CATALOGUE</li></a>
            <a href=panier><li>PANIER</li></a>
            <a href=deconnexion><li>SE DECONNECTER</li></a>
            <a href=profil><li>PROFIL $user</li></a>
        </ul>
    </nav>
   $content
   <footer>
        <div id=logo-footer>
            <img src=/www/leprieur1u/limaga-php/src/web/images/raccoon-logo.png width="60" height="50">
            <h3>Raccoon & Co</h3>
        </div>
        <div class="pied"> <h5> Copyright 2015-2016 - DAUDIER_LEPRIEUR_TRIVINO - Tous droits réservés </h5> </div>
   </footer>
</body>
</html>
END;

        echo $html;
    }
}