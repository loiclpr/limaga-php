<?php
/**
 * Created by PhpStorm.
 * User: Laura
 * Date: 15/01/2016
 * Time: 08:52
 */

namespace app\vues;

class VueInscription
{
    public function __construct()
    {

    }

    public function formulaire(){

        $html=<<<CT
<html>
<head>
    <title>Limaga</title>
    <link href=/www/leprieur1u/limaga-php/src/web/style.css rel=stylesheet type=text/css media=all>
    <meta charset="utf-8">
</head>
<body>
<nav>
    <ul>
        <span><a href=accueil><li><img src=/www/leprieur1u/limaga-php/src/web/images/logo-limaga.png></li></a></span>
        <a href=tarif><li>TARIF</li></a>
        <a href=catalogue><li>CATALOGUE</li></a>
        <a href=inscription><li>S'ENREGISTRER</li></a>
        <a href=connexion><li>SE CONNECTER</li></a>
    </ul>
</nav>
<div align="center">
    <h2>Inscription</h2>
    <br /><br />
    <form method="POST" action="">
        <table>
            <tr>
                <td align="right">
                    <label for="nom">Nom :</label>
                </td>
                <td>
                    <input type="text" required="Entrez votre nom" placeholder="Votre nom" id="nom" name="nom" />
                </td>
            </tr>

            <tr>
                <td align="right">
                    <label for="prenom">Prenom :</label>
                </td>
                <td>
                    <input type="text" required="Entrez votre prénom" placeholder="Votre prénom" id="prenom" name="prenom" />
                </td>
            </tr>


            <tr>
                <td align="right">
                    <label for="email">Email :</label>
                </td>
                <td>
                    <input type="email" required="Email requis" placeholder="Votre mail" id="email" name="email" />
                </td>
            </tr>

            <tr>
                <td align="right">
                    <label for="mdp">Mot de passe :</label>
                </td>
                <td>
                    <input type="password" required="mot de passe requis" placeholder="Votre mot de passe" id="mdp" name="mdp" />
                </td>
            </tr>

             <tr>
                <td align="right">
                    <label for="mdp2">Vérifier mot de passe :</label>
                </td>
                <td>
                    <input type="password" required="vérification du mot de passe requise" placeholder="Votre mot de passe confirmé" id="mdp2" name="mdp2" />
                </td>
            </tr>


            <tr>
                <td align="right">
                    <label for="niveau">Niveau :</label>
                </td>
                <td>
                    <input type="number" required="Indiquez votre niveau de natation" id="niveau" name="niveau" />
                </td>
            </tr>

            <tr>
                <td></td>
                <td align="center">
                    <br />
                    <input type="submit" name="forminscription" value="Je m'inscris" />
                </td>
            </tr>
        </table>
    </form>
</div>
<footer>
        <div id=logo-footer>
            <img src=/www/leprieur1u/limaga-php/src/web/images/raccoon-logo.png width="60" height="50">
            <h3>Raccoon & Co</h3>
        </div>
        <div class="pied"> <h5> Copyright 2015-2016 - DAUDIER_LEPRIEUR_TRIVINO - Tous droits réservés </h5> </div>
   </footer>
</body>
</html>
CT;
        echo $html;
    }

}