<?php
/**
 * Created by PhpStorm.
 * User: pierre
 * Date: 11/01/2016
 * Time: 20:23
 */

namespace app\vues;
define('PATH_ROOT', explode('index.php', \Slim\Slim::getInstance()->request->getRootUri())[0]);
class VuePanier
{
    private $tab;

    public function __construct($tab_obj){
        $this->tab = $tab_obj;
    }

    public function render() {
        if($this->tab[0] == "vous n'avez commandé aucun article"){
            $content = "<div>". $this->tab[0] .  "</div>" .
            "<form role=\"for\" method=\"POST\" action=>
             <button type=\"submit\" > Retour au catalogue </button>
             </form>
             // les liens snt ic parce que le bouton ne marchais pas, pensez a les enlever et utiliser le bouton
             <a href=\"/www/leprieur1u/limaga-php/index.php/catalogue\">lien catalogue</a>";
        }
        else{
            $total = 0;
            $content = "<div>";
            $entier = 0;
            foreach($this->tab as $v){
                if($entier%2 == 0){
                    $content = $content . " " . $v->nom . " " . $v->descritpion . " " . "quantite : " . $this->tab[$entier + 1]. " prix : " .$v->prix . "euros";
                    $total = $total + $v->prix *$this->tab[$entier + 1];
                }
                $entier++;
            }
            $content = $content .  "<div>" . " prix : ". $total . "euros</div>" .
            "<form role=\"for\" method=\"POST\" action=\"http://localhost/limaga-php/index.php/paiement\">
             <button type=\"submit\" name=\"payer\"> Payer </button>
             </form>
             <a href=\"/www/leprieur1u/limaga-php/index.php/paiement\">lien paiement</a>";
        }

        $user = '';
        if(isset($_SESSION['idProfil'])){
            $user = $_SESSION['idProfil'];
        }

        $html = <<<END
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <title>Limaga</title>
    <link href=/www/leprieur1u/limaga-php/src/web/style.css rel=stylesheet type=text/css media=all>
<body>
    <nav>
        <ul>
            <span><a href=accueil><li><img src=/www/leprieur1u/limaga-php/src/web/images/logo-limaga.png></li></a></span>
            <a href=tarif><li>TARIF</li></a>
            <a href=catalogue><li>CATALOGUE</li></a>
            <a href=panier><li>PANIER</li></a>
            <a href=deconnexion><li>SE DECONNECTER</li></a>
            <a href=profil><li>PROFIL $user</li></a>
        </ul>
    </nav>
   $content
    <footer>
        <div id=logo-footer>
            <img src=/www/leprieur1u/limaga-php/src/web/images/raccoon-logo.png width="60" height="50">
            <h3>Raccoon & Co</h3>
        </div>
        <div class="pied"> <h5> Copyright 2015-2016 - DAUDIER_LEPRIEUR_TRIVINO - Tous droits réservés </h5> </div>
    </footer>
</body>
</html>
END;

        echo $html;
    }
}