<?php
/**
 * Created by PhpStorm.
 * User: Loic
 * Date: 15/01/2016
 * Time: 08:37
 */

namespace app\vues;
define('PATH_ROOT', explode('index.php', \Slim\Slim::getInstance()->request->getRootUri())[0]);
class VueProfil{
    private $tab;
    private $URL;

    public function __construct($tab_obj){
        $this->tab = $tab_obj;
        $this->URL = PATH_ROOT;
    }

    public function render() {
        $content = "";
        foreach($this->tab as $attribut=>$valeur){
            $content .= "<article><h3>$attribut</h3>";
            $content .= "$valeur</article>";
        }

        $user = '';
        if(isset($_SESSION['idProfil'])){
            $user = $_SESSION['idProfil'];
        }
        $html = <<<END
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <title>Limaga</title>
    <link href=/www/leprieur1u/limaga-php/src/web/style.css rel=stylesheet type=text/css media=all>
</head>
<body>
    <nav>
        <ul>
            <span><a href=accueil><li><img src=/www/leprieur1u/limaga-php/src/web/images/logo-limaga.png></li></a></span>
            <a href=tarif><li>TARIF</li></a>
            <a href=catalogue><li>CATALOGUE</li></a>
            <a href=panier><li>PANIER</li></a>
            <a href=deconnexion><li>SE DECONNECTER</li></a>
            <a href=profil><li>PROFIL $user</li></a>
        </ul>
    </nav>
    <div class=main>
        $content
    </div>
    <footer>
        <div id=logo-footer>
            <img src=/www/leprieur1u/limaga-php/src/web/images/raccoon-logo.png width="60" height="50">
            <h3>Raccoon & Co</h3>
        </div>
        <div class="pied"> <h5> Copyright 2015-2016 - DAUDIER_LEPRIEUR_TRIVINO - Tous droits réservés </h5> </div>
    </footer>
</body>
</html>
END;

        echo $html;
    }
}